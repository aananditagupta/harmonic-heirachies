#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofBackground(0);

    myMaterial.setDiffuseColor(ofColor(128, 0, 200));
    myMaterial.setSpecularColor(ofColor(255, 255, 255));
    myMaterial.setAmbientColor(ofColor(128, 0, 255));
    myMaterial.setShininess(50.0);
    
    myMaterial2.setDiffuseColor(ofColor(51,153,255));
    myMaterial2.setSpecularColor(ofColor(255, 255, 255));
    myMaterial2.setAmbientColor(ofColor(128, 0, 255));
    myMaterial2.setShininess(25.0);

    myPointLight.setDiffuseColor(ofColor(255, 255, 255));
    myPointLight.setSpecularColor(ofColor(255, 255, 255));
    myPointLight.setPosition(vec3(10, 10, 20));

    ofSetGlobalAmbientColor(ofColor(60, 60, 60));

    myCamera.setPosition(vec3(0, 0, 20));
    myCamera.setTarget(vec3(0, 0, 0));
    myCamera.setFov(60.0f);
    myCamera.setAutoDistance(false);
    
    baseFrequency = 0.2;
    maxBranchDepth = 24;
    scaleMult = 0.8;
    scaleAmplitude = 0.15;
    rotAmplitude = 40.0;
    phaseShift = 30.0;
    harmonicShift = 1;
}

//--------------------------------------------------------------
void ofApp::update(){

}

//--------------------------------------------------------------
void ofApp::draw(){
    ofEnableDepthTest();
    ofEnableLighting();
    myCamera.begin();

    myPointLight.enable();
//  ofDrawSphere(5.0f);
    float t = ofGetElapsedTimef();
    float theta = 360.0 * baseFrequency * t;
//    drawHarmonicHierarchy(theta, 0, 1, 0);
    int numInstances = 16;
    for (int i = 0; i < numInstances; i++) {
        ofPushMatrix();
        ofRotateZDeg(360.0 * i / numInstances);
        float theta = 360.0 * baseFrequency * t/2;
        float phase = 360.0 * i / numInstances;
//        myMaterial.begin();
//        drawHarmonicHierarchy_leftbranch(theta, phase, 1, 0);
//        myMaterial.end();
        myMaterial2.begin();
        drawHarmonicHierarchy_rightbranch(theta, phase, 1, 0);
        myMaterial2.end();
        ofPopMatrix();
    }

    myPointLight.disable();
    myCamera.end();
}

//--------------------------------------------------------------
void ofApp::drawHarmonicHierarchy_leftbranch(float theta, float phase, int harmonic, int branchDepth){
    ofPushMatrix();
    ofRotateZDeg(rotAmplitude * sin(ofDegToRad(harmonic * theta + phase)));
    ofScale(scaleMult + scaleAmplitude * cos(ofDegToRad(harmonic * theta + phase)));
    ofTranslate(vec3(0, 1, 0));
    ofDrawBox(1.0);
    ofTranslate(vec3(0, 1, 0));
    if(branchDepth < maxBranchDepth)
    {
        drawHarmonicHierarchy_leftbranch(theta, phase + phaseShift, harmonic + harmonicShift, branchDepth + 1);
//        myMaterial2.begin();
//        drawHarmonicHierarchy_rightbranch(theta, phase + phaseShift, harmonic + harmonicShift, branchDepth + 1);
//        myMaterial2.end();
    }
    ofPopMatrix();
}

//--------------------------------------------------------------
void ofApp::drawHarmonicHierarchy_rightbranch(float theta, float phase, int harmonic, int branchDepth){
    ofPushMatrix();
    ofRotateZDeg(rotAmplitude * sin(ofDegToRad(harmonic * theta + phase)));
    ofRotateXDeg(rotAmplitude * sin(ofDegToRad(harmonic * theta)));
    ofScale(scaleMult + scaleAmplitude * cos(ofDegToRad(harmonic * theta + phase)));
    ofTranslate(vec3(0, 1, 0));
    ofDrawBox(1.0);
    ofTranslate(vec3(1, 1, 0));
    if(branchDepth < maxBranchDepth)
    {
        myMaterial.begin();
        drawHarmonicHierarchy_leftbranch(theta, phase + phaseShift, harmonic + harmonicShift, branchDepth + 1);
        myMaterial.end();
        drawHarmonicHierarchy_rightbranch(theta, phase + phaseShift, harmonic + harmonicShift, branchDepth + 1);
        
    }
    ofPopMatrix();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
