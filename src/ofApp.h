#pragma once

#include "ofMain.h"

using namespace glm;

class ofApp : public ofBaseApp{

    public:
        void setup();
        void update();
        void draw();
        void drawHarmonicHierarchy_leftbranch(float theta, float phase, int harmonic, int branchDepth);
        void drawHarmonicHierarchy_rightbranch(float theta, float phase, int harmonic, int branchDepth);
        
        void keyPressed(int key);
        void keyReleased(int key);
        void mouseMoved(int x, int y );
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);
        void dragEvent(ofDragInfo dragInfo);
        void gotMessage(ofMessage msg);

        ofEasyCam myCamera;
        ofMaterial myMaterial;
        ofMaterial myMaterial2;
        ofLight myPointLight;
    
        float baseFrequency;    
        int maxBranchDepth;
        float scaleMult;
        float scaleAmplitude;
        float rotAmplitude;
        float phaseShift;
        int harmonicShift;
};
